module Main where

import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.Client
import Lib
import HttpBin

--main :: IO ()
--main = someFunc
main :: IO ()
main = do
  manager <- newManager defaultManagerSettings
  res <- runClientM queries (ClientEnv manager (BaseUrl Http "ip.jsontest.com" 80 ""))
  post <- runClientM (httpbinPost $ User "bob" ) (ClientEnv manager (BaseUrl Http "httpbin.org" 80 ""))
  case post of
    Left err -> putStrLn $ "Error: " ++ show err
    Right (ipa) -> do
      print ipa
