{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module HttpBin
    ( httpbinPost
    , User(User)
    ) where

import Data.Aeson
import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.Client

data User = User
          {name :: String} deriving(Show, Generic)

instance ToJSON User
instance FromJSON User

data HttpPostResponse = HttpPostResponse
  { form :: User
  } deriving (Show, Generic)

instance FromJSON HttpPostResponse

type API = "post" :> ReqBody '[JSON] User :> Post '[JSON] HttpPostResponse

api :: Proxy API
api = Proxy

httpbinPostApi :: User -> ClientM HttpPostResponse
httpbinPostApi = client api


httpbinPost :: User -> ClientM HttpPostResponse
httpbinPost user = do
  resp <- httpbinPostApi user
  return (resp)

--{
--  "args": {},
--  "data": "",
--  "files": {},
--  "form": {
--    "{\"name\":\"bob\"}": ""
--  },
--  "headers": {
--    "Accept": "*/*",
--    "Connection": "close",
--    "Content-Length": "14",
--    "Content-Type": "application/x-www-form-urlencoded",
--    "Host": "httpbin.org",
--    "User-Agent": "curl/7.54.0"
--  },
--  "json": null,
--  "origin": "73.14.230.13",
--  "url": "http://httpbin.org/post"
--}

