{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}

module Lib
    ( someFunc,
      queries
    ) where

import Data.Aeson
import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.Client

someFunc :: IO ()
someFunc = putStrLn "someFunc"

data IpAddress = IpAddress
  { ip :: String
  } deriving (Show, Generic)

instance FromJSON IpAddress

type API = Get '[JSON] IpAddress

api :: Proxy API
api = Proxy

ipAddress = client api


queries :: ClientM (IpAddress)
queries = do
  ip <- ipAddress
  return (ip)


